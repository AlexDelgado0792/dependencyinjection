﻿
using System;
using DemoLibrary.Utilities;

namespace DemoLibrary
{
    public class BetterBusinessLogic : IBusinessLogic
    {
        private ILogger _logger;
        private IDataAccess _dataAccess;

        public BetterBusinessLogic(ILogger logger, IDataAccess dataAccess)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }

        public void ProcessData()
        {
            _logger.Log("Starting processing data.");
            Console.WriteLine();
            Console.WriteLine("Processing Data");
            _dataAccess.LoadData();
            _dataAccess.SaveData("Processed Info");
            Console.WriteLine();
            _logger.Log("Finished processing data.");
            Console.WriteLine();
        }
    }
}
