﻿
using System;
using DemoLibrary.Utilities;

namespace DemoLibrary
{
    public class BusinessLogic : IBusinessLogic
    {
        private ILogger _logger;
        private IDataAccess _dataAccess;

        public BusinessLogic(ILogger logger, IDataAccess dataAccess)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }

        public void ProcessData()
        {
            _logger.Log("Starting processing data.");
            Console.WriteLine("Processing Data");
            _dataAccess.LoadData();
            _dataAccess.SaveData("Processed Info");
            _logger.Log("Finished processing data.");
        }
    }
}
